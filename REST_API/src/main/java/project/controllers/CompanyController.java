package project.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.dto.CompanyDTO;
import project.models.Company;
import project.services.ICompanyService;
import project.support.CompanyConverter;

@RestController
@RequestMapping("/company")
public class CompanyController {

	private ICompanyService companyService;
	private CompanyConverter companyConverter;

	public CompanyController(ICompanyService companyService, CompanyConverter companyConverter) {
		this.companyConverter = companyConverter;
		this.companyService = companyService;
	}

	@GetMapping("/getCompanies")
	public List<CompanyDTO> getAllCompanies() {
		List<Company> companies = companyService.getAllCompanies();
		return companyConverter.createFromEntites(companies);
	}

	@PostMapping("/create")
	public void createCompany(@RequestBody CompanyDTO dto) {
		Company company = companyConverter.dtoToModel(dto);
		 companyService.createCompany(company);
		
	}

	@DeleteMapping("/delete/{id}")
	public void deleteCompanyById(@PathVariable int id) {
		companyService.deleteCompany(id);
	
	}

	@GetMapping("/get/{city}")
	public List<CompanyDTO> getCompanyByCity(@PathVariable String city) {
		List<Company> companies = companyService.getCompaniesByCity(city);
		return companyConverter.createFromEntites(companies);
	}

}
