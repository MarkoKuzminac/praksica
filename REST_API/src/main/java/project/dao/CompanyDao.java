package project.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import project.models.Company;
@Repository
public class CompanyDao extends AbstractJpaDao<Company> {

	@PersistenceContext
	private EntityManager entityManager;
	
	public CompanyDao(){
	      setClazz(Company.class );
	   }
	
	public List<Company> getCompaniesByCity(String name) {
		Query nativeQuery=entityManager.createNativeQuery("SELECT * FROM company WHERE city=?",Company.class);
		nativeQuery.setParameter( 1, name );
		List<Company> companiesFromThatCity= (List<Company>)nativeQuery.getResultList();
		return companiesFromThatCity;
		}
}
