package project.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import project.models.Department;

@Repository
public class DepartmentDao extends AbstractJpaDao<Department> {

	@PersistenceContext
	EntityManager entityManager;

	public DepartmentDao() {
		setClazz(Department.class);
	}

	public Department getDepByName(String name) {
		Query q = entityManager.createNativeQuery("SELECT * FROM department where name=?",Department.class);
		q.setParameter(1, name);
		Department d = (Department) q.getSingleResult();
		return d;
	}

	public void deleteDepartment(String name) {
		Query q = entityManager.createNativeQuery("DELETE  FROM department where name=?");
		q.setParameter(1, name);
		q.executeUpdate();
	}

	public void changeName(String oldName, String newName) {
		Query q = entityManager.createNativeQuery("UPDATE department SET name=? WHERE name=?",Department.class);
		q.setParameter(1, newName);
		q.setParameter(2, oldName);
		q.executeUpdate();
	}

	public int getEmployeeCountByDepartment(String department) {
		Query q = entityManager.createNativeQuery("SELECT employees_count FROM department WHERE name=?");
		q.setParameter(1, department);
		return (int) q.getSingleResult();
	}
}
