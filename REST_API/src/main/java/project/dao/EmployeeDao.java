package project.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import project.models.Employee;
@Repository
public class EmployeeDao extends AbstractJpaDao<Employee> {

	@PersistenceContext
	private EntityManager em;
	
	public EmployeeDao(){
	      setClazz(Employee.class );
	   }
	
	public Employee getHighestSalary() {
		Query q=em.createNativeQuery("SELECT * FROM employee WHERE salary>= ALL ( SELECT salary FROM "
				+ "employee)",Employee.class);
		Employee e=(Employee) q.getSingleResult();
		return e;
	}
	public void changeEmployeesDepartment(int employee_id,int dep_id) {
		Query q=em.createNativeQuery("UPDATE employee SET department_id=? WHERE employee_id=?");
		q.setParameter(1, dep_id);
		q.setParameter(2, employee_id);
		q.executeUpdate();
	}
	public void deleteEmployeesByDepartment(int department_id) {
		Query q=em.createNativeQuery("DELETE * FROM employee WHERE department_id=?");
		q.setParameter(1, department_id);
		q.executeUpdate();	}
}
