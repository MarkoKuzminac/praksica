package project.dto;

import project.models.Department;

public class EmployeeDTO {

	private String fullName;
	private String phoneNumber;
	private String email;
	private double salary;
	

	public EmployeeDTO() {
	}

	public EmployeeDTO(String fullname, String phone, String email, double salary) {
		
		this.fullName = fullname;
		this.phoneNumber = phone;
		this.email = email;
		this.salary = salary;
		

	}

	

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	

	

}