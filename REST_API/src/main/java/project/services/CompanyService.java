package project.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.dao.CompanyDao;
import project.models.Company;

@Service
public class CompanyService implements ICompanyService {

	@Autowired
	private CompanyDao dao;

	
	public List<Company> getAllCompanies() {
		return dao.findAll();
	}

	@Transactional
	public void createCompany(Company company) {
		dao.create(company);
	}

	@Transactional
	public void deleteCompany(int id) {
		dao.deleteById(id);
	}

	
	public List<Company> getCompaniesByCity(String city) {
		return dao.getCompaniesByCity(city);
	}
}
