package project.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.dao.DepartmentDao;
import project.models.Department;

@Service
public class DepartmentService implements IDepartmentService {

	@Autowired
	private DepartmentDao dao;

	@Transactional
	public void createDepartment(Department department) {
		dao.create(department);
	}

	@Transactional
	public void deleteDepartmentByName(String name) {
		dao.deleteDepartment(name);
	}

	@Transactional
	public void changeDepartmentName(String oldName, String newName) {
		dao.changeName(oldName, newName);
	}

	public List<Department> getAllDepartments() {
		return dao.findAll();
	}

	public Department getDepartmentByName(String name) {
		return dao.getDepByName(name);

	}

	public int getEmployeeCountByDepartment(String department) {
		return dao.getEmployeeCountByDepartment(department);
	}

}
