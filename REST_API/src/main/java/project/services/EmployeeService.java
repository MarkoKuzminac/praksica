package project.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.dao.EmployeeDao;
import project.models.Employee;

@Service
public class EmployeeService implements IEmployeeService {

	@Autowired
	private EmployeeDao dao;

	@Transactional
	public void createEmployee(Employee employee) {
		dao.create(employee);
	}

	public Employee getEmployeeById(int id) {
		return dao.findOne(id);
	}

	@Transactional
	public void deleteEmployeeById(int id) {
		dao.deleteById(id);
	}

	@Transactional
	public void deleteEmployeesByDepartment(int department_id) {
		dao.deleteEmployeesByDepartment(department_id);
	}

	public List<Employee> getAllEmployees() {
		return dao.findAll();
	}

	public Employee getHighestSalary() {
		return dao.getHighestSalary();
	}

	@Transactional
	public void changeEmployeesDepartment(int employee_id, int dep_id) {
		dao.changeEmployeesDepartment(employee_id, dep_id);
	}

}
