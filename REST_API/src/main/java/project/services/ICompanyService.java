package project.services;

import java.util.List;

import project.models.Company;

public interface ICompanyService {

	List<Company> getAllCompanies();

	void createCompany(Company company);

	void deleteCompany(int id);

	List<Company> getCompaniesByCity(String city);

}