package project.services;

import java.util.List;

import project.models.Department;

public interface IDepartmentService {

	void createDepartment(Department department);

	void deleteDepartmentByName(String name);

	void changeDepartmentName(String oldName, String newName);

	List<Department> getAllDepartments();

	Department getDepartmentByName(String name);

	int getEmployeeCountByDepartment(String department);

}