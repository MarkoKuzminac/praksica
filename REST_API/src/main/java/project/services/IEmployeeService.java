package project.services;

import java.util.List;

import project.models.Employee;

public interface IEmployeeService {

	void createEmployee(Employee employee);

	Employee getEmployeeById(int id);

	void deleteEmployeeById(int id);

	void deleteEmployeesByDepartment(int department_id);

	List<Employee> getAllEmployees();

	Employee getHighestSalary();

	void changeEmployeesDepartment(int emp_id, int dep_id);

}