package project.support;

import org.springframework.stereotype.Component;

import project.dto.EmployeeDTO;
import project.models.Employee;

@Component
public class EmployeeConverter implements GenericConverter<EmployeeDTO, Employee> {

	public EmployeeDTO modelToDto(Employee employee) {
		EmployeeDTO dto = new EmployeeDTO(employee.getFullName(), employee.getPhoneNumber(), employee.getEmail(),
				employee.getSalary());
		return dto;
	}

	public Employee dtoToModel(EmployeeDTO dto) {
		Employee entity = new Employee(dto.getFullName(), dto.getPhoneNumber(), dto.getEmail(),
				dto.getSalary());
		return entity;
	}

}
