package project.support;

import java.util.ArrayList;
import java.util.List;

public interface GenericConverter<T1, T2> {

	T2 dtoToModel(T1 t1);

	T1 modelToDto(T2 t2);

	default List<T1> createFromEntites(final List<T2> entities) {
		List<T1> dtoList = new ArrayList<T1>();
		for (T2 entity : entities) {
			dtoList.add(modelToDto(entity));
		}

		return dtoList;
	}

	default List<T2> createFromDtos(final List<T1> dtos) {
		List<T2> entitiesList = new ArrayList<T2>();
		for (T1 dto : dtos) {
			entitiesList.add(dtoToModel(dto));
		}

		return entitiesList;

	}

}
