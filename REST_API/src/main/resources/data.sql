insert into company(name,country,city,address) values
('Pennsylvania','Cranston','USA','Dunder Mifflin'),('SValley','LA','USA','Pied Piper');
insert into department(name,company_id) values
('HR','1'),('SALES','1'),('DEVELOPMENT','2'),('MARKETING','2');
insert into employee(full_name,email,salary,department_id) values
('Dwight Schrute','dwight@dm.com',90000.55,1),('Pam Beasley','pam@dm.com',77777.77,1),
('Jim Halpert','jim@dm.com',120000.99,2),('Michael Scott','michael@dm.com',133123.44,2),
('Gilfoyle Linux','gilfoyle@pp.com',149000.50,3),('Dinesh Hindu','dinesh@dm.com',55600.55,3),
('Erlich Bachman','bachman@dm.com',0.00,4),('Yin Yang','yang@dm.com',0.00,4);


