
create table company(id int primary key auto_increment, name nvarchar(100), country nvarchar(100), city nvarchar(100), address nvarchar(100));
create table department(id int primary key auto_increment, name nvarchar(100), employees_count int unsigned, company_id int, foreign key(company_id) references company(id));
create table employee(id int primary key auto_increment, full_name nvarchar(100), phone_number varchar(20), email nvarchar(50), salary double, department_id int, foreign key(department_id) references department(id));