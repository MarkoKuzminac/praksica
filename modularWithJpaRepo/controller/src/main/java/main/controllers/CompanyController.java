package main.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import main.converters.CompanyConverter;
import main.data.models.Company;
import main.dto.CompanyDTO;
import main.service.ICompanyService;

@RestController
@RequestMapping("/company")
public class CompanyController {

	private ICompanyService companyService;
	private CompanyConverter companyConverter;

	@Autowired
	public CompanyController(ICompanyService companyService, CompanyConverter companyConverter) {
		this.companyConverter = companyConverter;
		this.companyService = companyService;
	}

	@GetMapping("/getCompanies")
	public List<CompanyDTO> getAllCompanies() {
		List<Company> companies = companyService.getAllCompanies();
		return companyConverter.createFromEntites(companies);
	}

	@PostMapping("/create")
	public void createCompany(@RequestBody CompanyDTO dto) {
		Company company = companyConverter.dtoToModel(dto);
		companyService.createCompany(company);

	}

	@DeleteMapping("/delete/{id}")
	public void deleteCompanyById(@PathVariable Long id) {
		companyService.deleteCompany(id);

	}

	@GetMapping("/get/{city}")
	public List<CompanyDTO> getCompanyByCity(@PathVariable String city) {
		List<Company> companies = companyService.getCompaniesByCity(city);
		return companyConverter.createFromEntites(companies);
	}

}
