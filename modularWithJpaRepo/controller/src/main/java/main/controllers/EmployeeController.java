package main.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import main.converters.EmployeeConverter;
import main.data.models.Employee;
import main.dto.EmployeeDTO;
import main.service.IEmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	private IEmployeeService employeeService;
	private EmployeeConverter employeeConverter;

	@Autowired
	public EmployeeController(IEmployeeService employeeService, EmployeeConverter employeeConverter) {
		this.employeeConverter = employeeConverter;
		this.employeeService = employeeService;
	}

	@GetMapping("/get/{id}")
	public EmployeeDTO getEmployeeById(@PathVariable Long id) {

		return employeeConverter.modelToDto(employeeService.getEmployeeById(id));
	}

	@PostMapping("/create")
	public void createEmployee(@RequestBody EmployeeDTO dto) {
		Employee newEmployee = employeeConverter.dtoToModel(dto);
		employeeService.createEmployee(newEmployee);

	}

	@DeleteMapping("/delete/{id}")
	public void deleteEmployeeById(@PathVariable Long id) {

		employeeService.deleteEmployeeById(id);

	}

	@DeleteMapping("/deleteDepartment/{id}")
	public void deleteByDepartmentId(@PathVariable Long id) {
		employeeService.deleteEmployeesByDepartment(id);
	}

	@GetMapping("/employees")
	public List<EmployeeDTO> getAllEmployees() {
		List<Employee> employees = employeeService.getAllEmployees();

		return employeeConverter.createFromEntites(employees);
	}

	@GetMapping("/empWithHighestSalary")
	public EmployeeDTO getEmployeeWithHighestSalary() {

		Employee employee = employeeService.getHighestSalary();
		return employeeConverter.modelToDto(employee);
	}

	@PutMapping("/changeDepartment/{emp_id}/{dep_id}")
	public void changeDepartment(@PathVariable Long emp_id, @PathVariable Long dep_id) {
		employeeService.changeEmployeesDepartment(emp_id, dep_id);
	}

}
