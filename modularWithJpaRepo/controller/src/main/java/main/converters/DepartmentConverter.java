package main.converters;

import org.springframework.stereotype.Component;

import main.data.models.Department;
import main.dto.DepartmentDTO;

@Component
public class DepartmentConverter implements GenericConverter<DepartmentDTO, Department> {

	@Override
	public Department dtoToModel(DepartmentDTO dto) {
		Department dep = new Department(dto.getName(), dto.getEmployeesCount());
		return dep;
	}

	@Override
	public DepartmentDTO modelToDto(Department dep) {
		DepartmentDTO dto = new DepartmentDTO(dep.getName(), dep.getEmployeesCount());
		return dto;
	}

}
