package main.converters;

import org.springframework.stereotype.Component;

import main.data.models.Employee;
import main.dto.EmployeeDTO;

@Component
public class EmployeeConverter implements GenericConverter<EmployeeDTO, Employee> {

	public EmployeeDTO modelToDto(Employee employee) {
		EmployeeDTO dto = new EmployeeDTO(employee.getFullName(), employee.getPhoneNumber(), employee.getEmail(),
				employee.getSalary());
		return dto;
	}

	public Employee dtoToModel(EmployeeDTO dto) {
		Employee entity = new Employee(dto.getFullName(), dto.getPhoneNumber(), dto.getEmail(), dto.getSalary());
		return entity;
	}

}
