package main.dto;

public class DepartmentDTO {
	
	private String name;
	private int employeesCount;

	public DepartmentDTO() {
	}

	public DepartmentDTO(String name, int employeesCount) {

		
		this.name = name;
		this.employeesCount = employeesCount;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEmployeesCount() {
		return employeesCount;
	}

	public void setEmployeesCount(int employeesCount) {
		this.employeesCount = employeesCount;
	}

}
