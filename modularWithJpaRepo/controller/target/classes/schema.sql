
create table company(id integer primary key auto_increment, name nvarchar(100), country nvarchar(100), city nvarchar(100), address nvarchar(100));
create table department(id integer primary key auto_increment, name nvarchar(100), employees_count integer unsigned, company_id integer, foreign key(company_id) references company(id));
create table employee(id integer primary key auto_increment, full_name nvarchar(100), phone_number varchar(20), email nvarchar(50), salary decimal, department_id integer, foreign key(department_id) references department(id));