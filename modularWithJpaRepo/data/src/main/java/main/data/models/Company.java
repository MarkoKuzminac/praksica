package main.data.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "company")
public class Company implements Serializable {
	private static final long serialVersionUID = 3L;
	@Id
	@Column(name = "id")
	private Long id;
	@Column
	@NotNull
	private String name;
	@Column
	private String country;
	@Column
	private String city;
	@Column
	private String address;
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
	private List<Department> departments;

	protected Company() {
	}

	public Company(String name, String country, String city, String address) {

		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

	public Company(Long id, String name, String country, String city, String address) {

		this.id = id;
		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	@Override
	public String toString() {
		return "Company{" + "id=" + id + ", name='" + name + '\'' + ", country='" + country + '\'' + ", city='" + city
				+ '\'' + ", address='" + address + '\'' + ", departments=" + departments + '}';
	}
}
