package main.data.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "department")
public class Department implements Serializable {
	private static final long serialVersionUID = 2L;
	@Id
	@Column(name = "id")
	private Long id;
	@Column
	@NotNull
	private String name;
	@Column
	private int employeesCount;
	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
	private List<Employee> employees;
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	protected Department() {
	}

	public Department(String name, int employees_count) {
		this.name = name;
		this.employeesCount = employees_count;

	}

	public Department(Long id, String name, int employees_count) {

		this.id = id;
		this.name = name;
		this.employeesCount = employees_count;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEmployeesCount() {
		return employeesCount;
	}

	public void setEmployeesCount(int employeesCount) {
		this.employeesCount = employeesCount;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void addEmployee(Employee e) {
		employees.add(e);
	}

	@Override
	public String toString() {
		return "Department{" + "id=" + id + ", name='" + name + '\'' + ", employeesCount=" + employeesCount
				+ ", employees=" + employees + '}';
	}
}
