package main.data.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import main.data.models.Company;
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

	List<Company> findByCity(String city);
	
	@Transactional
    @Modifying
	void deleteById(Long id);
	
	
	
	
}
