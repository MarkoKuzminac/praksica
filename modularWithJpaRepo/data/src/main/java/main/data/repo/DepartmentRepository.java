package main.data.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import main.data.models.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

	
	void deleteByName(String name);

	Department findByName(String name);

	@Transactional
    @Modifying
	@Query(value="update department d set d.name =:newName  where d.name =:oldName",nativeQuery = true)
	void updateDepartmentName(@Param("newName") String newName, @Param("oldName")String oldName);
	
	@Query(value="select employees_count from department  where name=:name",nativeQuery = true)
	int getCount(@Param("name") String name);
}
