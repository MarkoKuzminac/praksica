package main.data.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import main.data.models.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query(value="SELECT * FROM employee WHERE salary>= ALL ( SELECT salary FROM employee",nativeQuery = true)
	public Employee findByMaxSalary();

	@Transactional
    @Modifying
	@Query(value="UPDATE employee SET department_id=?1 WHERE employee_id=?2",nativeQuery = true)
	public void changeEmployeeDepartment(Long dep_id, Long emp_id);

	@Transactional
    @Modifying
	public void deleteByDepartment_id(Long id);
}
