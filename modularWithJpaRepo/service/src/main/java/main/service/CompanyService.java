package main.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.data.models.Company;
import main.data.repo.CompanyRepository;

@Service
public class CompanyService implements ICompanyService {

	private CompanyRepository companyRepo;

	@Autowired
	public CompanyService(CompanyRepository companyRepo) {

		this.companyRepo = companyRepo;
	}

	public List<Company> getAllCompanies() {

		return companyRepo.findAll();
	}

	@Transactional
	public void createCompany(Company company) {
		companyRepo.save(company);
	}

	@Transactional
	public void deleteCompany(Long id) {
		companyRepo.deleteById(id);
	}

	public List<Company> getCompaniesByCity(String city) {
		return companyRepo.findByCity(city);
	}
}
