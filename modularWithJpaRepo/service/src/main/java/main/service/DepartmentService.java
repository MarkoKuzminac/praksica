package main.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.data.models.Department;
import main.data.repo.DepartmentRepository;

@Service
public class DepartmentService implements IDepartmentService {

	private DepartmentRepository depRepo;

	@Autowired
	public DepartmentService(DepartmentRepository depRepo) {

		this.depRepo = depRepo;
	}

	@Transactional
	public void createDepartment(Department department) {
		depRepo.save(department);
	}

	@Transactional
	public void deleteDepartmentByName(String name) {
		depRepo.deleteByName(name);
	}

	@Transactional
	public void changeDepartmentName(String oldName, String newName) {
		depRepo.updateDepartmentName(newName, oldName);
	}

	public List<Department> getAllDepartments() {
		return depRepo.findAll();
	}

	public Department getDepartmentByName(String name) {
		return depRepo.findByName(name);

	}

	public int getEmployeeCountByDepartment(String name) {
		return depRepo.getCount(name);
	}

}
