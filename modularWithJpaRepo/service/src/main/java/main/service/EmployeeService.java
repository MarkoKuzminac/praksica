package main.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.data.models.Employee;
import main.data.repo.EmployeeRepository;

@Service
public class EmployeeService implements IEmployeeService {

	private EmployeeRepository empRepo;

	@Autowired
	public EmployeeService(EmployeeRepository empRepo) {

		this.empRepo = empRepo;
	}

	@Transactional
	public void createEmployee(Employee employee) {
		empRepo.save(employee);
	}

	public Employee getEmployeeById(Long id) {
		return empRepo.getOne(id);
	}

	@Transactional
	public void deleteEmployeeById(Long id) {
		empRepo.deleteById(id);
	}

	@Transactional
	public void deleteEmployeesByDepartment(Long department_id) {
		empRepo.deleteByDepartment_id(department_id);
	}

	public List<Employee> getAllEmployees() {
		return empRepo.findAll();
	}

	public Employee getHighestSalary() {
		return empRepo.findByMaxSalary();
	}

	@Transactional
	public void changeEmployeesDepartment(Long employee_id, Long dep_id) {
		empRepo.changeEmployeeDepartment(dep_id, employee_id);
	}

	

}
