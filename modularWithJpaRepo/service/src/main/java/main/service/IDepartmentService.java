package main.service;

import java.util.List;

import main.data.models.Department;

public interface IDepartmentService {

	void createDepartment(Department department);

	void deleteDepartmentByName(String name);

	void changeDepartmentName(String oldName, String newName);

	List<Department> getAllDepartments();

	Department getDepartmentByName(String name);

	int getEmployeeCountByDepartment(String department);

}