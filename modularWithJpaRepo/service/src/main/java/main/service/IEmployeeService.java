package main.service;

import java.util.List;

import main.data.models.Employee;

public interface IEmployeeService {

	void createEmployee(Employee employee);

	Employee getEmployeeById(Long id);

	void deleteEmployeeById(Long id);

	void deleteEmployeesByDepartment(Long department_id);

	List<Employee> getAllEmployees();

	Employee getHighestSalary();

	void changeEmployeesDepartment(Long emp_id, Long dep_id);

}