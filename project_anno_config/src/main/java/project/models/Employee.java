package project.models;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;


public class Employee {
    private int id;
    private String fullName;
    private String phoneNumber;
    private String email;
    private double salary;
    private Department department;

    

    	
   
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id= " + id +
                
                ", fullName='" + fullName + 
                " salary= " +salary +
                '\'' + "department=" + department +
                '}';
    }
}
