package project.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import project.models.Company;

@Repository("companyRepository")
public class CompanyRepository {

	private List<Company> companies = new ArrayList<>();
	private DepartmentRepository dr = new DepartmentRepository();

	public List<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	public CompanyRepository() {
		Company dunderMifflin = new Company();
		dunderMifflin.setId(1);
		dunderMifflin.setName("Dunder Mifflin");
		dunderMifflin.setCountry("USA");
		dunderMifflin.setCity("Cranston");
		dunderMifflin.setAddress("Penny, Pennsylvania");
		dunderMifflin.setDepartments(dr.getDepartments());

		Company hooli = new Company();
		hooli.setId(2);
		hooli.setName("Hooli");
		hooli.setCountry("USA");
		hooli.setCity("LA");
		hooli.setAddress("Sillicon Valley");
		hooli.setDepartments(dr.getDepartments());

		Company c = new Company();
		c.setAddress("DanilaKisa3");
		c.setId(10);
		c.setName("Bright Marbles");
		c.setCountry("Serbia");
		c.setCity("NoviSad");

		companies.add(dunderMifflin);
		companies.add(hooli);
	}
	public void createCompany(Company company){
        companies.add(company);
    }
	public void deleteCompany(int id){
        Company company = companies.stream().filter(c->c.getId()==id).findFirst().get();
        companies.remove(company);
    }
	public List<Company> getCompaniesByCity(String city){
        return companies.stream().filter(c->c.getCity().equals(city)).collect(Collectors.toList());
    }
}
