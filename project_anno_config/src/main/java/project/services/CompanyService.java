package project.services;

import project.models.Company;
import project.repositories.CompanyRepository;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value="companyService")
public class CompanyService implements ICompanyService {

	private CompanyRepository companyRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see project.services.ICompanyService#getCompanyRepository()
	 */
	
	
	@Autowired
	public void setCompanyRepository(CompanyRepository companyRepository) {
		this.companyRepository = companyRepository;
	}

	@Override
	public List<Company> getAllCompanies() {
		return companyRepository.getCompanies();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see project.services.ICompanyService#createCompany(project.models.Company)
	 */
	@Override
	public void createCompany(Company company) {
		companyRepository.getCompanies().add(company);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see project.services.ICompanyService#deleteCompany(int)
	 */
	@Override
	public void deleteCompany(int id) {
		 companyRepository.deleteCompany(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see project.services.ICompanyService#getCompaniesByCity(java.lang.String)
	 */
	@Override
	public List<Company> getCompaniesByCity(String city) {
		  return companyRepository.getCompaniesByCity(city);
	}
}
