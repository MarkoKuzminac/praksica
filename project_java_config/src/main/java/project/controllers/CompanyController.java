package project.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.dto.CompanyDTO;
import project.models.Company;
import project.services.ICompanyService;
import project.support.CompanyConverter;

@RestController
@RequestMapping("/company")
public class CompanyController {

	private ICompanyService companyService;
	private CompanyConverter companyConverter;

	public CompanyController(ICompanyService companyService, CompanyConverter companyConverter) {
		this.companyConverter = companyConverter;
		this.companyService = companyService;
	}

	@GetMapping("/companies")
	public List<CompanyDTO> getAllCompanies() {
		List<Company> companies = companyService.getAllCompanies();
		return companyConverter.createFromEntites(companies);
	}

	@PostMapping("/create")
	public CompanyDTO createCompany(@RequestBody CompanyDTO dto) {
		Company company = companyConverter.dtoToModel(dto);
		Company insertedCompany = companyService.createCompany(company);
		return companyConverter.modelToDto(insertedCompany);
	}

	@DeleteMapping("/delete/{id}")
	public CompanyDTO deleteCompanyById(@PathVariable int id) {
		Company company = companyService.deleteCompany(id);
		return companyConverter.modelToDto(company);
	}

	@GetMapping("/{city}")
	public List<CompanyDTO> getCompanyByCity(@PathVariable String city) {
		List<Company> companies = companyService.getCompaniesByCity(city);
		return companyConverter.createFromEntites(companies);
	}

}
