package project.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.dto.DepartmentDTO;
import project.models.Department;
import project.models.Employee;
import project.services.IDepartmentService;
import project.support.DepartmentConverter;

@RestController
@RequestMapping("/department")
public class DepartmentController {

	private IDepartmentService departmentService;
	private DepartmentConverter departmentConverter;

	public DepartmentController(IDepartmentService departmentService, DepartmentConverter departmentConverter) {
		this.departmentConverter = departmentConverter;
		this.departmentService = departmentService;
	}

	@PostMapping("/create")
	public DepartmentDTO createDepartment(@RequestBody DepartmentDTO dto) {
		Department newDep = departmentConverter.dtoToModel(dto);
		Department dep = departmentService.createDepartment(newDep);
		return departmentConverter.modelToDto(dep);
	}

	@DeleteMapping("/delete/{name}")
	public DepartmentDTO deleteDepartment(@PathVariable String name) {

		Department dep = departmentService.deleteDepartment(name);
		return departmentConverter.modelToDto(dep);
	}

	@GetMapping("/departments")
	public List<DepartmentDTO> getAllDepartments() {
		List<Department> deps = departmentService.getAllDepartments();
		return departmentConverter.createFromEntites(deps);
	}

	@GetMapping("/{name}")
	public DepartmentDTO getDepartmentByName(@PathVariable String name) {

		return departmentConverter.modelToDto(departmentService.getDepartmentByName(name));
	}

	@PutMapping("/{oldName}/{newName}")
	public DepartmentDTO changeDepartmentName(@PathVariable String oldName, @PathVariable String newName) {

		Department dep = departmentService.changeDepartmentName(oldName, newName);
		return departmentConverter.modelToDto(dep);
	}
}
