package project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import project.dto.EmployeeDTO;
import project.models.Employee;
import project.services.IEmployeeService;
import project.support.EmployeeConverter;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	private IEmployeeService employeeService;
	private EmployeeConverter employeeConverter;

	public EmployeeController(IEmployeeService employeeService, EmployeeConverter employeeConverter) {
		this.employeeConverter = employeeConverter;
		this.employeeService = employeeService;
	}

	@GetMapping("/{id}")
	public EmployeeDTO getEmployeeById(@PathVariable int id) {

		return employeeConverter.modelToDto(employeeService.getEmployeeById(id));
	}

	@PostMapping("/create")
	public EmployeeDTO createEmployee(@RequestBody EmployeeDTO dto) {
		Employee newEmployee = employeeConverter.dtoToModel(dto);
		Employee emp = employeeService.createEmployee(newEmployee);
		return employeeConverter.modelToDto(emp);
	}

	@DeleteMapping("/delete/{id}")
	public EmployeeDTO deleteEmployeeById(@PathVariable int id) {

		Employee emp = employeeService.deleteEmployeeById(id);
		return employeeConverter.modelToDto(emp);
	}

	@GetMapping("/employees")
	public List<EmployeeDTO> getAllEmployees() {
		List<Employee> employees = employeeService.getAllEmployees();
		return employeeConverter.createFromEntites(employees);
	}

	@GetMapping("/highestSalary")
	public EmployeeDTO getEmployeeWithHighestSalary() {

		Employee employee = employeeService.getHighestSalary();
		return employeeConverter.modelToDto(employee);
	}

}
