package project.dto;

public class DepartmentDTO {
	private int id;
	private String name;
	private int employeesCount;

	public DepartmentDTO() {
	}

	public DepartmentDTO(int id, String name, int employeesCount) {

		this.id = id;
		this.name = name;
		this.employeesCount = employeesCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEmployeesCount() {
		return employeesCount;
	}

	public void setEmployeesCount(int employeesCount) {
		this.employeesCount = employeesCount;
	}

}
