package project.dto;

import project.models.Department;

public class EmployeeDTO {

	private int id;
	private String fullName;
	private String phoneNumber;
	private String email;
	private double salary;
//	private Department department;

	public EmployeeDTO() {
	}

	public EmployeeDTO(int id,String fullname, String phone, String email, double salary) {
		 this.id = id;
		this.fullName = fullname;
		this.phoneNumber = phone;
		this.email = email;
		this.salary = salary;
	//	this.department=department;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	

}