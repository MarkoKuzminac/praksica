package project.models;

import java.util.List;

public class Department {
	private int id;
	private String name;
	private int employeesCount;
	private List<Employee> employees;

	public Department() {
	}

	public Department(int id, String name, int employeesCount) {

		this.id = id;
		this.name = name;
		this.employeesCount = employeesCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEmployeesCount() {
		return employeesCount;
	}

	public void setEmployeesCount(int employeesCount) {
		this.employeesCount = employeesCount;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void addEmployee(Employee e) {
		employees.add(e);
	}

	@Override
	public String toString() {
		return "Department{" + "id=" + id + ", name='" + name + '\'' + ", employeesCount=" + employeesCount
				+ ", employees=" + employees + '}';
	}
}
