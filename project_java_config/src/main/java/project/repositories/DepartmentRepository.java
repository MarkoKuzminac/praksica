package project.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import project.models.Department;
import project.models.Employee;

@Repository
public class DepartmentRepository {

	private List<Department> departments = new ArrayList<>();
	private EmployeeRepository emplRepo;

	public DepartmentRepository(EmployeeRepository employeeRepository) {
		this.emplRepo = employeeRepository;
		Department it = new Department();
		it.setId(1);
		it.setName("IT");
		it.setEmployeesCount(50);
		it.setEmployees(emplRepo.getEmployees().stream().filter(e -> e.getDepartment().getName().equals("IT"))
				.collect(Collectors.toList()));

		Department hr = new Department();
		hr.setId(2);
		hr.setName("HR");
		hr.setEmployeesCount(50);
		hr.setEmployees(emplRepo.getEmployees().stream().filter(e -> e.getDepartment().getName().equals("HR"))
				.collect(Collectors.toList()));

		Department acc = new Department();
		acc.setId(3);
		acc.setEmployeesCount(100);
		acc.setName("Accounting");
		acc.setEmployees(emplRepo.getEmployees().stream().filter(e -> e.getDepartment().getName().equals("Accounting"))
				.collect(Collectors.toList()));
		departments.add(hr);
		departments.add(it);
		departments.add(acc);

	}

	public Department createDepartment(Department department) {
		departments.add(department);
		return department;
	}

	public Department deleteDepartment(String name) {
		Department dep = departments.stream().filter(d -> d.getName().equals(name)).findFirst().get();
		departments.remove(dep);
		return dep;
	}

	public Department changeDepartmentName(String oldName, String newName) {
		Department d = departments.stream().filter(dep -> dep.getName().equals(oldName)).findFirst().orElse(null);
		if (d != null) {
			d.setName(newName);
			return d;
		}
		return null;
	}

	public Department getDepartmentByName(String name) {
		return departments.stream().filter(d -> d.getName().equals(name)).findFirst().orElse(null);

	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public void changeEmployeesDepartment(int id, String depName) {

		for (Employee e : emplRepo.getEmployees()) {
			if (e.getId() == id) {
				getDepartmentByName(depName).addEmployee(e);

				break;
			}
		}
	}

}
