package project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.models.Company;
import project.repositories.CompanyRepository;

@Service
public class CompanyService implements ICompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	// @Override
	/// public void setCompanyRepository(CompanyRepository companyRepository) {
	// this.companyRepository = companyRepository;
	// }

	@Override
	public List<Company> getAllCompanies() {
		return companyRepository.getCompanies();
	}

	@Override
	public Company createCompany(Company company) {
		companyRepository.getCompanies().add(company);
		return company;
	}

	@Override
	public Company deleteCompany(int id) {
		return companyRepository.deleteCompany(id);
	}

	@Override
	public List<Company> getCompaniesByCity(String city) {
		return companyRepository.getCompaniesByCity(city);
	}
}
