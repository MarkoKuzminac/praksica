package project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.models.Department;
import project.repositories.DepartmentRepository;

@Service
public class DepartmentService implements IDepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	// public void setDepartmentRepository(DepartmentRepository
	// departmentRepository) {
	// this.departmentRepository = departmentRepository;
	// }

	@Override
	public Department createDepartment(Department department) {
		return departmentRepository.createDepartment(department);
	}

	@Override
	public Department deleteDepartment(String name) {
		return departmentRepository.deleteDepartment(name);
	}

	@Override
	public Department changeDepartmentName(String oldName, String newName) {
		return departmentRepository.changeDepartmentName(oldName, newName);
	}

	@Override
	public List<Department> getAllDepartments() {
		return departmentRepository.getDepartments();
	}

	@Override
	public Department getDepartmentByName(String name) {
		return departmentRepository.getDepartmentByName(name);

	}

	public void changeEmployeesDepartment(int id, String dep) {
		departmentRepository.changeEmployeesDepartment(id, dep);
	}

}
