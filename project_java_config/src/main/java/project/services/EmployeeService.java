package project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.models.Department;
import project.models.Employee;
import project.repositories.EmployeeRepository;

@Service
public class EmployeeService implements IEmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	public EmployeeRepository getEmployeeRepository() {
		return employeeRepository;
	}

	// public void setEmployeeRepository(EmployeeRepository employeeRepository) {
	// this.employeeRepository = employeeRepository;
	// }

	@Override
	public Employee createEmployee(Employee employee) {
		return employeeRepository.createEmployee(employee);
	}

	public Employee getEmployeeById(int id) {
		return employeeRepository.getEmployeeById(id);
	}

	@Override
	public Employee deleteEmployeeById(int id) {
		return employeeRepository.deleteEmployeeById(id);
	}
	
	@Override
	public void deleteEmployeesByDepartment(String department) {
		employeeRepository.deleteEmployeesByDepartment(department);
	}

	@Override
	public List<Employee> getAllEmployees() {
		return employeeRepository.getEmployees();
	}

	@Override
	public int getEmployeeCountByDepartment(String department) {
		return employeeRepository.getEmployeeCountByDepartment(department);
	}

	@Override
	public Employee getHighestSalary() {
		return employeeRepository.getHighestSalary();
	}

	
	
}
