package project.services;

import java.util.List;

import project.models.Company;
import project.repositories.CompanyRepository;

public interface ICompanyService {

	// void setCompanyRepository(CompanyRepository companyRepository);

	List<Company> getAllCompanies();

	Company createCompany(Company company);

	Company deleteCompany(int id);

	List<Company> getCompaniesByCity(String city);

}