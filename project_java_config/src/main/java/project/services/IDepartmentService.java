package project.services;

import java.util.List;

import project.models.Department;

public interface IDepartmentService {

	Department createDepartment(Department department);

	Department deleteDepartment(String name);

	Department changeDepartmentName(String oldName, String newName);

	void changeEmployeesDepartment(int id, String dep);

	List<Department> getAllDepartments();

	Department getDepartmentByName(String name);

}