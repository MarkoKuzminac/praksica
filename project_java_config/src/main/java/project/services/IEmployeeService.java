package project.services;

import java.util.List;

import project.models.Department;
import project.models.Employee;

public interface IEmployeeService {

	Employee createEmployee(Employee employee);
	
	Employee getEmployeeById(int id);

	Employee deleteEmployeeById(int id);

	void deleteEmployeesByDepartment(String department);

	List<Employee> getAllEmployees();

	int getEmployeeCountByDepartment(String department);

	Employee getHighestSalary();

	//void changeEmployeesDepartment(int id, Department dep);

}