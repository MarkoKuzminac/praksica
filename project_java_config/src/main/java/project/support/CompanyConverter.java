package project.support;

import org.springframework.stereotype.Component;

import project.dto.CompanyDTO;
import project.models.Company;

@Component
public class CompanyConverter implements GenericConverter<CompanyDTO, Company> {

	@Override
	public Company dtoToModel(CompanyDTO dto) {
		Company company = new Company(dto.getId(), dto.getName(), dto.getCountry(), dto.getCity(), dto.getAddress());
		return company;
	}

	@Override
	public CompanyDTO modelToDto(Company company) {
		CompanyDTO dto = new CompanyDTO(company.getId(), company.getName(), company.getCountry(), company.getCity(),
				company.getAddress());
		return dto;
	}

}
