package project.support;

import org.springframework.stereotype.Component;

import project.dto.DepartmentDTO;
import project.models.Department;

@Component
public class DepartmentConverter implements GenericConverter<DepartmentDTO, Department> {

	@Override
	public Department dtoToModel(DepartmentDTO dto) {
		Department dep = new Department(dto.getId(), dto.getName(), dto.getEmployeesCount());
		return dep;
	}

	@Override
	public DepartmentDTO modelToDto(Department dep) {
		DepartmentDTO dto = new DepartmentDTO(dep.getId(), dep.getName(), dep.getEmployeesCount());
		return dto;
	}

}
