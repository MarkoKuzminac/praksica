package project;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import project.models.Department;
import project.models.Employee;
import project.services.CompanyService;
import project.services.DepartmentService;
import project.services.EmployeeService;
import project.services.ICompanyService;
import project.services.IDepartmentService;
import project.services.IEmployeeService;

public class Main {
    
    public static void main(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        IDepartmentService depService = ctx.getBean("departmentService", DepartmentService.class);
		System.out.println(depService.getDepartmentByName("Accounting"));
		depService.deleteDepartment("Accounting");
		depService.changeDepartmentName("HR", "Human Resources");
		System.out.println(depService.getAllDepartments());

		IEmployeeService emplService = ctx.getBean("employeeService", EmployeeService.class);
		System.out.println(emplService.getEmployeeCountByDepartment("IT"));
		System.out.println(emplService.getHighestSalary());
		emplService.deleteEmployeeById(1);
		emplService.deleteEmployeesByDepartment("IT");
		// emplService.changeEmployeesDepartment(1, acc);
		System.out.println(emplService.getAllEmployees());

		ICompanyService compservice = ctx.getBean("companyService", CompanyService.class);
		compservice.deleteCompany(2);
		System.out.println(compservice.getAllCompanies());
		System.out.println(compservice.getCompaniesByCity("LA"));

		ctx.close();
    }
}
