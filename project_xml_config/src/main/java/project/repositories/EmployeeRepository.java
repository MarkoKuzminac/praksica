package project.repositories;

import project.models.Department;
import project.models.Employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRepository {

    private List<Employee> employees = new ArrayList<>();


    public EmployeeRepository(){
    	Department it = new Department();
        it.setName("IT");
        Department hr = new Department();
        hr.setName("HR");
        Employee dwight = new Employee();
        dwight.setId(1);
        dwight.setFullName("Dwight Schrute");
        dwight.setPhoneNumber("022-666-555");
        dwight.setEmail("dwight@jackhammer.com");
        dwight.setSalary(900000);
        dwight.setDepartment(hr);

        Employee gaevin = new Employee();
        gaevin.setId(2);
        gaevin.setFullName("Gaevin Belson");
        gaevin.setPhoneNumber("022-111-333");
        gaevin.setEmail("gevin@hoolie.com");
        gaevin.setSalary(190000);
        gaevin.setDepartment(it);

        Employee pam = new Employee();
        pam.setId(3);
        pam.setFullName("Pam Beasley");
        pam.setPhoneNumber("021-434-633");
        pam.setEmail("pam@beasley.com");
        pam.setSalary(80000);
        pam.setDepartment(hr);

        Employee gilfoyle = new Employee();
        gilfoyle.setId(4);
        gilfoyle.setFullName("Gilfoyle Only");
        gilfoyle.setPhoneNumber("021-456-123");
        gilfoyle.setEmail("gilfoyle@svalley.com");
        gilfoyle.setSalary(270000);
        gilfoyle.setDepartment(it);
        
        
        Employee michael = new Employee();
        michael.setFullName("Michael Scott");
        michael.setId(5);
        michael.setEmail("scott@michael.com");
        michael.setPhoneNumber("069-222-444");
        michael.setSalary(15000);

        
        employees.add(dwight);
        employees.add(pam);
        employees.add(gaevin);
        employees.add(gilfoyle);

    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
    public void deleteEmployeeById(int id) {
    	for(Employee e:employees) {
    		if (e.getId()==id) {
    	
        employees.remove(e);
        break;}}
    }
    public void deleteEmployeesByDepartment(String department){
        for(int i=0; i<employees.size(); i++) {
        	if(employees.get(i).getDepartment().getName().equals(department)) {
        		employees.remove(i);
        	}
       }
    	employees.removeIf(e->e.getDepartment().getName().equals(department));
    }
    public int getEmployeeCountByDepartment(String department){
    	int temp=0;
    	for(Employee e:employees) {
    		if(e.getDepartment().getName().equals(department)) {
    			temp++;
    		}
    	}
    	return temp;
    }
    public Employee getHighestSalary(){
        return employees.stream().max((e1, e2)->Integer.compare((int)e1.getSalary(), (int)e2.getSalary())).get();
    }
    
    
    public void changeEmployeesDepartment(int id, Department dep){
        
        for(Employee e:employees) {
        	if(e.getId()==id) {
        		e.setDepartment(dep);
        		break;
        	}
        }
    }

    
}
