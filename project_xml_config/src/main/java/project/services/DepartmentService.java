package project.services;

import project.models.Department;
import project.repositories.DepartmentRepository;
import project.repositories.EmployeeRepository;

import java.util.List;
import java.util.stream.Collectors;

public class DepartmentService implements IDepartmentService {

    private DepartmentRepository departmentRepository;

    public void setDepartmentRepository(DepartmentRepository departmentRepository) {
		this.departmentRepository = departmentRepository;
	}
	/* (non-Javadoc)
	 * @see project.services.IDepartmentService#createDepartment(project.models.Department)
	 */
    @Override
	public void createDepartment(Department department){
        departmentRepository.createDepartment(department);
    }
    /* (non-Javadoc)
	 * @see project.services.IDepartmentService#deleteDepartment(project.models.Department)
	 */
    @Override
	public void deleteDepartment(String name){
       departmentRepository.deleteDepartment(name);
    }
    /* (non-Javadoc)
	 * @see project.services.IDepartmentService#changeDepartmentName(java.lang.String, java.lang.String)
	 */
    @Override
	public void changeDepartmentName(String oldName, String newName){
        departmentRepository.changeDepartmentName(oldName, newName);
        }
    
    /* (non-Javadoc)
	 * @see project.services.IDepartmentService#getAllDepartments()
	 */
    @Override
	public List<Department> getAllDepartments(){
        return departmentRepository.getDepartments();
    }
    /* (non-Javadoc)
	 * @see project.services.IDepartmentService#getDepartmentByName(java.lang.String)
	 */
    @Override
	public Department getDepartmentByName(String name){
        return departmentRepository.getDepartmentByName(name);
    
    }
}
