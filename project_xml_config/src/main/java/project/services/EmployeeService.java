package project.services;

import project.models.Department;
import project.models.Employee;
import project.repositories.DepartmentRepository;
import project.repositories.EmployeeRepository;

import java.util.Comparator;
import java.util.List;

public class EmployeeService implements IEmployeeService {

    private EmployeeRepository employeeRepository;

	public void setEmployeeRepository(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	/* (non-Javadoc)
	 * @see project.services.IEmployeeService#createEmployee(project.models.Employee)
	 */
    @Override
	public void createEmployee(Employee employee){
        employeeRepository.getEmployees().add(employee);
    }
    /* (non-Javadoc)
	 * @see project.services.IEmployeeService#deleteEmployee(int)
	 */
    @Override
	public void deleteEmployeeById(int id){
     employeeRepository.deleteEmployeeById(id);
    	
    }
    /* (non-Javadoc)
	 * @see project.services.IEmployeeService#changeEmplDepartment(int, project.models.Department)
	 */
    
    /* (non-Javadoc)
	 * @see project.services.IEmployeeService#deleteEmployeesByDepartment(project.models.Department)
	 */
    @Override
	public void deleteEmployeesByDepartment(String department){
       employeeRepository.deleteEmployeesByDepartment(department);
    }
    /* (non-Javadoc)
	 * @see project.services.IEmployeeService#getAllEmployees()
	 */
    @Override
	public List<Employee> getAllEmployees(){
        return employeeRepository.getEmployees();
    }
    /* (non-Javadoc)
	 * @see project.services.IEmployeeService#getEmployeeCountByDepartment(project.models.Department)
	 */
    @Override
	public int getEmployeeCountByDepartment(String department){
    	
    	return employeeRepository.getEmployeeCountByDepartment(department);
    }
   
   // }
    /* (non-Javadoc)
	 * @see project.services.IEmployeeService#getHighestSalary()
	 */
    @Override
	public Employee getHighestSalary(){
        return employeeRepository.getHighestSalary();
    }
    /* (non-Javadoc)
	 * @see project.services.IEmployeeService#changeEmployeesDepartment(int, project.models.Department)
	 */
    @Override
	public void changeEmployeesDepartment(int id, Department dep){
       employeeRepository.changeEmployeesDepartment(id, dep);
    }
}
