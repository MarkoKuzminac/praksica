package project.services;

import java.util.List;

import project.models.Department;
import project.models.Employee;

public interface IEmployeeService {

	void createEmployee(Employee employee);

	void deleteEmployeeById(int id);

	void deleteEmployeesByDepartment(String department);

	List<Employee> getAllEmployees();

	int getEmployeeCountByDepartment(String department);

	Employee getHighestSalary();

	void changeEmployeesDepartment(int id, Department dep);

}