package main;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = {"jpa.properties","service.properties"})
public class ServiceConfiguration {

}
