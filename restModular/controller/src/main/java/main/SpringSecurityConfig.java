package main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import main.service.EmployeeService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	EmployeeService employeeService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(employeeService)
		.passwordEncoder(new BCryptPasswordEncoder());
	//	.passwordEncoder(passwordEncoder());
		//auth.inMemoryAuthentication()
		//.withUser("dwight") .password("root")
       // .roles("USER")
		//.and().withUser("jim").password("root").roles("USER","DBADMIN");
		
		
	}
	@Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
	@Bean
    public TokenStore tokenStore() {
        return new InMemoryTokenStore();
    }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		//.antMatchers("/").permitAll()
				// .antMatchers("/employee/get/all", "/department/get/all", "/company/get/all").permitAll()
				//.antMatchers("/employee/get/**", "/department/get/**", "/company/get/**").hasRole("USER")
				// .antMatchers("/**").hasRole("DBADMIN")
				//.antMatchers("/employee/get/all").hasAnyRole("DBADMIN")
				.anyRequest().authenticated()
			//	.and().httpBasic()
		//		.and()
		//		.formLogin()
           //     .and()
          //  .logout()
           // 	.logoutUrl("/logout")
           //     .deleteCookies("JESSIONID")
           //     .invalidateHttpSession(true)    
            //    .logoutSuccessUrl("/logout-success")
				.and().csrf().disable();
	}
	
	//public PasswordEncoder passwordEncoder(){
	//	PasswordEncoder encoder = new BCryptPasswordEncoder();
	///	return encoder;
  //  }

	//@Bean
	//public static NoOpPasswordEncoder passwordEncoder() {
	//	return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
	//}

}
