package main.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import main.converters.DepartmentConverter;
import main.data.models.Department;
import main.dto.DepartmentDTO;
import main.service.IDepartmentService;

@RestController
@RequestMapping("/department")
public class DepartmentController {

	private IDepartmentService departmentService;
	private DepartmentConverter departmentConverter;

	@Autowired
	public DepartmentController(IDepartmentService departmentService, DepartmentConverter departmentConverter) {
		this.departmentConverter = departmentConverter;
		this.departmentService = departmentService;
	}

	@PreAuthorize("hasRole('ROLE_USER','ROLE_DBADMIN')")
	@GetMapping("/get/count/{dep}")
	public Long getEmployeesCount(@PathVariable String dep) {
		return departmentService.getEmployeeCountByDepartment(dep);
	}

	@PreAuthorize("permitAll")
	@GetMapping("/get/all")
	public List<DepartmentDTO> getAllDepartments() {
		List<Department> deps = departmentService.getAllDepartments();
		return departmentConverter.createFromEntites(deps);
	}

	@PreAuthorize("hasRole('ROLE_USER','ROLE_DBADMIN')")
	@GetMapping("/get/{name}")
	public DepartmentDTO getDepartmentByName(@PathVariable String name) {
		return departmentConverter.modelToDto(departmentService.getDepartmentByName(name));
	}

	@PreAuthorize("hasRole('ROLE_DBADMIN')")
	@PostMapping("/create")
	public void createDepartment(@RequestBody DepartmentDTO dto) {
		Department newDep = departmentConverter.dtoToModel(dto);
		departmentService.createDepartment(newDep);

	}

	@PreAuthorize("hasRole('ROLE_DBADMIN')")
	@DeleteMapping("/delete/{name}")
	public void deleteDepartmentByName(@PathVariable String name) {

		departmentService.deleteDepartmentByName(name);

	}

	@PreAuthorize("hasRole('ROLE_DBADMIN')")
	@PutMapping("/update/name/{oldName}/{newName}")
	public void changeDepartmentName(@PathVariable String oldName, @PathVariable String newName) {

		departmentService.changeDepartmentName(oldName, newName);

	}

}
