package main.controllers;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import main.converters.EmployeeConverter;
import main.data.models.Employee;
import main.dto.EmployeeDTO;
import main.service.IEmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	// @Autowired
	// private SessionRegistry sr;
	private IEmployeeService employeeService;
	private EmployeeConverter employeeConverter;

	@Autowired
	public EmployeeController(IEmployeeService employeeService, EmployeeConverter employeeConverter) {
		this.employeeConverter = employeeConverter;
		this.employeeService = employeeService;
	}

	
      

	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_DBADMIN')")
	@GetMapping("/get/{id}")
	public EmployeeDTO getEmployeeById(@PathVariable Long id) {

		return employeeConverter.modelToDto(employeeService.getEmployeeById(id));
	}

	@PreAuthorize("permitAll")
	@GetMapping("/get/all")
	public List<EmployeeDTO> getAllEmployees() {
		List<Employee> employees = employeeService.getAllEmployees();

		return employeeConverter.createFromEntites(employees);
	}

	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_DBADMIN')")
	@GetMapping("/get/bySalary")
	public EmployeeDTO getEmployeeWithHighestSalary() {

		Employee employee = employeeService.getHighestSalary();
		return employeeConverter.modelToDto(employee);
	}

	@PreAuthorize("hasRole('ROLE_DBADMIN')")
	@PostMapping("/create")
	public void createEmployee(@RequestBody EmployeeDTO dto) {
		Employee newEmployee = employeeConverter.dtoToModel(dto);
		employeeService.createEmployee(newEmployee);

	}

	@PreAuthorize("hasRole('ROLE_DBADMIN')")
	@DeleteMapping("/delete/{id}")
	public void deleteEmployeeById(@PathVariable Long id) {

		employeeService.deleteEmployeeById(id);

	}

	@PreAuthorize("hasRole('ROLE_DBADMIN')")
	@DeleteMapping("/delete/department/{id}")
	public void deleteByDepartmentId(@PathVariable Long id) {
		employeeService.deleteEmployeesByDepartment(id);
	}

	@PreAuthorize("hasRole('ROLE_DBADMIN')")
	@PutMapping("/update/department/{emp_id}/{dep_id}")
	public void changeDepartment(@PathVariable Long emp_id, @PathVariable Long dep_id) {
		employeeService.changeEmployeesDepartment(emp_id, dep_id);
	}

}
