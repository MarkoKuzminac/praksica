package main.controllers;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@FrameworkEndpoint
public class OAuthController {

	@Autowired
    private TokenStore tokenStore;
	
	@Autowired
    ConsumerTokenServices tokenServices;

    @DeleteMapping("/oauth/token")
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpServletRequest request) {
    	  Collection<OAuth2AccessToken> tokens = tokenStore.findTokensByClientId("jim-client");
          for (OAuth2AccessToken token : tokens) {
              tokenServices.revokeToken(token.getValue());
          }
           
        }
    
}
