package main.converters;

import org.springframework.stereotype.Component;

import main.data.models.Company;
import main.dto.CompanyDTO;

@Component
public class CompanyConverter implements GenericConverter<CompanyDTO, Company> {

	@Override
	public Company dtoToModel(CompanyDTO dto) {
		if (dto.getId() == null) {
			Company company = new Company(dto.getName(), dto.getCity(), dto.getCountry(), dto.getAddress());
			return company;
		}
		Company company = new Company(dto.getId(), dto.getName(), dto.getCity(), dto.getCountry(), dto.getAddress());
		return company;
	}

	@Override
	public CompanyDTO modelToDto(Company company) {
		CompanyDTO dto = new CompanyDTO(company.getName(), company.getCity(), company.getCountry(),
				company.getAddress());
		return dto;
	}

}
