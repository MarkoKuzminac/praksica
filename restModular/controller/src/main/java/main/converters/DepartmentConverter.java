package main.converters;

import org.springframework.stereotype.Component;

import main.data.models.Department;
import main.dto.DepartmentDTO;

@Component
public class DepartmentConverter implements GenericConverter<DepartmentDTO, Department> {

	@Override
	public Department dtoToModel(DepartmentDTO dto) {
		if(dto.getId()==null) {
		Department dep = new Department(dto.getName(), dto.getEmployees_count());
		return dep;
		}
		Department dep = new Department(dto.getId(),dto.getName(), dto.getEmployees_count());
		return dep;
		
	}

	@Override
	public DepartmentDTO modelToDto(Department dep) {
		DepartmentDTO dto = new DepartmentDTO(dep.getName(), dep.getEmployeesCount());
		return dto;
	}

}
