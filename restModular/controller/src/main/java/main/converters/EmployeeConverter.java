package main.converters;

import org.springframework.stereotype.Component;

import main.data.models.Employee;
import main.dto.EmployeeDTO;

@Component
public class EmployeeConverter implements GenericConverter<EmployeeDTO, Employee> {

	public EmployeeDTO modelToDto(Employee employee) {
		EmployeeDTO dto = new EmployeeDTO(employee.getFullName(), employee.getPhoneNumber(), employee.getEmail(),
				employee.getSalary());
		return dto;
	}

	public Employee dtoToModel(EmployeeDTO dto) {
		if(dto.getId()==null) {
		Employee entity = new Employee(dto.getFullName(),dto.getUsername(),dto.getPassword(),dto.getPhoneNumber(), dto.getSalary());
		return entity;
	}
		Employee entity = new Employee(dto.getId(),dto.getFullName(),dto.getUsername(),dto.getPassword(),dto.getPhoneNumber(), dto.getSalary());
		//Employee entity = new Employee(dto.getId(),dto.getFullName(),dto.getUsername(),dto.getPassword(), dto.getEmail(), dto.getSalary());
		return entity;
}
}