package main.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class CompanyDTO {
	private Long id;
	private String name;
	private String country;
	private String city;
	private String address;

	public CompanyDTO(String name, String city, String country, String address) {

		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

	public CompanyDTO(Long id, String name, String city, String country, String address) {
		this.id = id;
		this.name = name;
		this.city = city;
		this.country = country;
		this.address = address;
	}

	
	

}