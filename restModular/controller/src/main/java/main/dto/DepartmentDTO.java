package main.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DepartmentDTO {
	private Long id;
	private String name;
	private Long employees_count;

	public DepartmentDTO(Long id, String name, Long employees_count) {

		this.id = id;
		this.name = name;
		this.employees_count = employees_count;
	}

	public DepartmentDTO(String name, Long employees_count) {

		this.name = name;
		this.employees_count = employees_count;
	}

}
