package main.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import main.data.models.Role;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeDTO {

	private Long id;
	private String fullName;
	private String username;
	private String password;
	private String phoneNumber;
	private String email;
	private BigDecimal salary;

	private List<Role> roles;

	public String getUsername() {
		return username;
	}

	public EmployeeDTO(Long id, String fullName, String username, String password, List<Role> roles, String phoneNumber,
			String email, BigDecimal salary) {
		this.id = id;
		this.fullName = fullName;
		this.username = username;
		this.password = password;
		this.roles = roles;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.salary = salary;
	}

	public EmployeeDTO(Long id, String fullname, String phone, String email, BigDecimal salary) {
		this.id = id;
		this.fullName = fullname;
		this.phoneNumber = phone;
		this.email = email;
		this.salary = salary;

	}

	public EmployeeDTO(String fullName, String phoneNumber, String email, BigDecimal salary) {

		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.salary = salary;
	}

}