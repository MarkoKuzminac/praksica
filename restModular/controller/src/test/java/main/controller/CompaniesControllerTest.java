package main.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import main.controllers.CompanyController;
import main.converters.CompanyConverter;
import main.data.models.Company;
import main.service.ICompanyService;

@RunWith(SpringJUnit4ClassRunner.class)
//@WebMvcTest(CompanyController.class)
public class CompaniesControllerTest {

	
	private MockMvc mockMvc;

	@Mock
	private ICompanyService companiesService;
	@Mock
	private CompanyConverter companyConverter;

	@InjectMocks
	private CompanyController companyController;

	@org.junit.Before
	public void setUp() throws Exception {
		companyController = new CompanyController(companiesService, companyConverter);
		mockMvc = MockMvcBuilders.standaloneSetup(companyController).build();
	}

	@Test
	public void testGetAll() throws Exception {
		mockMvc.perform(get("/company/get/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@org.junit.Test
	public void testGetCompanyByCity() throws Exception {
		mockMvc.perform(get("/company/get/byCity/{city}", "Seattle").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testInsertCompany() throws Exception {
		Company apple = new Company("Apple Inc", "LA", "USA", "adress1");
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(apple);
		mockMvc.perform(post("/company/create").contentType(MediaType.APPLICATION_JSON).content(json)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@org.junit.Test
	public void testDeleteCompany() throws Exception {
		mockMvc.perform(delete("/company/delete/{id}", 1)).andExpect(status().isOk());
	}
}