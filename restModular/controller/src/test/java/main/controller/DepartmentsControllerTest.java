package main.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import main.controllers.DepartmentController;
import main.converters.DepartmentConverter;
import main.data.models.Department;
import main.service.IDepartmentService;

@RunWith(SpringJUnit4ClassRunner.class)
public class DepartmentsControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private DepartmentController departmentsController;
    @Mock
    private DepartmentConverter departmentConverter;
    @Mock
    private IDepartmentService departmentsService;

    

    @Before
    public void setUp() throws Exception {
        departmentsController = new DepartmentController(departmentsService, departmentConverter);
        mockMvc = MockMvcBuilders.standaloneSetup(departmentsController).build();
    }

    @Test
    public void testGetAll() throws Exception{
        mockMvc.perform(get("/department/get/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testGetByName() throws Exception{
        mockMvc.perform(get("/department/get/{name}", "IT").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testCreateNew() throws Exception{
        Department hr = new Department( "HR", 100l);
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(hr);
        mockMvc.perform(post("/department/create").contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testDeleteDepartment() throws Exception{
        mockMvc.perform(delete("/department/delete/{name}", "HR")).andExpect(status().isOk());
    }

    @Test
    public void testChangeName() throws Exception{
        mockMvc.perform(put("/department/update/name/{oldtName}/{newName}", "HR", "Human Resources")).andExpect(status().isOk());
    }
}