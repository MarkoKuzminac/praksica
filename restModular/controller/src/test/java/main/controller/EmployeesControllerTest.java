package main.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import main.controllers.EmployeeController;
import main.converters.EmployeeConverter;
import main.data.models.Employee;
import main.service.IEmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeesControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private EmployeeController employeesController;

	@Mock
	private IEmployeeService employeesService;
	@Mock
	private EmployeeConverter employeeConverter;

	@Before
	public void setUp() throws Exception {
		employeesController = new EmployeeController(employeesService, employeeConverter);
		mockMvc = MockMvcBuilders.standaloneSetup(employeesController).build();
	}

	@Test
	public void testGetAll() throws Exception {
		mockMvc.perform(get("/employee/get/all").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void testCreateNew() throws Exception {
		Employee dwight = new Employee("Dwight Schrutte", "dwight", "root", "dwight@amazon.com", new BigDecimal(10000));
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(dwight);
		mockMvc.perform(post("/employee/create").contentType(MediaType.APPLICATION_JSON).content(json)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void testDeleteEmployee() throws Exception {
		mockMvc.perform(delete("/employee/delete/{id}", 3)).andExpect(status().isOk());
	}

	@Test
	public void testDeleteByDepartment() throws Exception {
		mockMvc.perform(delete("/employee/delete/department/{id}", 2)).andExpect(status().isOk());
	}

	@Test
	public void testChangeDepartment() throws Exception {
		mockMvc.perform(put("/employee/update/department/{empid}/{depid}", 1, 4)).andExpect(status().isOk());
	}

	@Test
	public void testGetEmployeeWithHighestSalary() throws Exception {
		mockMvc.perform(get("/employee/get/bySalary").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
}