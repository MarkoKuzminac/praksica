package main.data.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "company")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Company implements Serializable {
	private static final long serialVersionUID = 3L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	@NotNull
	private String name;
	@Column
	private String city;
	@Column
	private String country;
	@Column
	private String address;
	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
	private List<Department> departments;

	public Company(String name, String city, String country, String address) {

		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

	public Company(Long id, String name, String city, String country, String address) {

		this.id = id;
		this.name = name;
		this.country = country;
		this.city = city;
		this.address = address;
	}

}
