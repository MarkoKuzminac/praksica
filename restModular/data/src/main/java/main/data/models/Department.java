package main.data.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "department")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Department implements Serializable {
	private static final long serialVersionUID = 2L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column
	@NotNull
	private String name;
	@Column(name = "employees_count")
	private Long employeesCount;
	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
	private List<Employee> employees;
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public Department(String name, Long employees_count) {
		this.name = name;
		this.employeesCount = employees_count;

	}

	public Department(Long id, String name, Long employees_count) {

		this.id = id;
		this.name = name;
		this.employeesCount = employees_count;

	}
}
