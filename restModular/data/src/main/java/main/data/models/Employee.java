package main.data.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "employee")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "full_name")
	private String fullName;
	@Column(name = "username", nullable = false, unique = true)
	private String username;
	@Column(name = "password")
	private String password;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "employee_role", joinColumns = { @JoinColumn(name = "employee_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private List<Role> roles;
	@Column(name = "phone_number")
	private String phoneNumber;
	@Column(name = "email")
	private String email;
	@Column(name = "salary")
	private BigDecimal salary;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_id")
	private Department department;

	public Employee(Long id, String fullName, String username, String password, String email, BigDecimal salary) {

		this.id = id;
		this.fullName = fullName;
		this.username = username;
		this.password = password;
		// this.roles = roles;
		this.email = email;
		this.salary = salary;
	}

	public Employee(String fullName, String username, String password, String email, BigDecimal salary) {

		// this.roles = roles;
		this.fullName = fullName;
		this.username = username;
		// this.password=password;
		// this.phoneNumber = phoneNumber;
		this.email = email;
		this.salary = salary;
		// this.department = dep;
	}

	public Employee(Long id, String fullName, String phoneNumber, String email, BigDecimal salary) {

		this.id = id;
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.salary = salary;
	}

	public Employee(Long id, String fullName, String phoneNumber, String email, BigDecimal salary, Department dep) {

		this.id = id;
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.salary = salary;
		this.department = dep;
	}
}
