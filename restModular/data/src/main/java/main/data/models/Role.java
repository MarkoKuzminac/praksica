package main.data.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Role {

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "role_name")
	private String roleName;

	@ManyToMany(mappedBy="roles",fetch=FetchType.EAGER)
	private List<Employee> employees;
	
	
	
	public Role() {}

	public Role(Long id, String roleName, List<Employee> employees) {

		this.id = id;
		this.roleName = roleName;
		this.employees = employees;
	}
	
	
	

	@Override
	public String toString() {
		return this.getRoleName() ;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
}
