package main.jpa;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import main.data.models.Department;
import main.data.models.Employee;
import main.data.models.Role;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	
	@Transactional
    @Modifying
	@Query(value="UPDATE employee SET password=?1",nativeQuery=true)
	public void setPassword(String password) ;
		
	

	@Query(value="SELECT * FROM employee WHERE salary>= ALL ( SELECT salary FROM employee)",nativeQuery = true)
	public Employee findByMaxSalary();

	@Transactional
    @Modifying
	@Query(value="UPDATE employee SET department_id=?1 WHERE id=?2",nativeQuery = true)
	public void changeEmployeeDepartment(Long dep_id, Long emp_id);

	@Transactional
    @Modifying
	public void deleteByDepartment_id(Long id);

	public Employee findByUsername(String username);
}
