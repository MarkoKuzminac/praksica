package main.testjpa;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import main.data.models.Company;
import main.jpa.CompanyRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@ContextConfiguration(classes = TestConfig.class)
public class CompanyRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private CompanyRepository repo;

	@Test
	public void whenFindByCityThenReturnCompanies() {
		Company company1 = new Company("Hooli", "Seattle", "USA", "adress");
		Company company2 = new Company("Google", "LA", "USA", "adress");
		Company company3 = new Company("Yahoo", "Seattle", "USA", "adress");
		entityManager.persist(company1);
		// entityManager.persist(company2);
		entityManager.persist(company3);
		// entityManager.flush();
		List<Company> found = repo.findByCity("Seattle");
		for (Company c : found)
			assertEquals(c.getCity(), "Seattle");
	}
	

}
