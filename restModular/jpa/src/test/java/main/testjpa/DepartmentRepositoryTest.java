package main.testjpa;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import main.data.models.Department;
import main.jpa.DepartmentRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@ContextConfiguration(classes = TestConfig.class)
public class DepartmentRepositoryTest {

	
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private DepartmentRepository repo;
	
	
	@Test
	public void getCount() {
		Department d1=new Department("IT",55L);
		entityManager.persist(d1);
		Long found=repo.getCount("IT");
		assertEquals(d1.getEmployeesCount(),found);
	}
}
