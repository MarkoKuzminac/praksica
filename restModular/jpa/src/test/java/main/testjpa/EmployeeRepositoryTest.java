package main.testjpa;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import main.data.models.Employee;
import main.jpa.EmployeeRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@ContextConfiguration(classes = TestConfig.class)
public class EmployeeRepositoryTest {

	

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EmployeeRepository repo;
	
	@Test
	public void findByUsernameTest() {
		Employee e1 = new Employee("Pera Peric", "user1", "password", "mail@live.com", new BigDecimal(22345.66));
		Employee e2 = new Employee("Zika Zikic", "user2", "password", "mail@live.com", new BigDecimal(99999.55));
		entityManager.persist(e1);
		entityManager.persist(e2);
		Employee found=repo.findByUsername("user2");
		assertEquals(e2,found);
	}
}
