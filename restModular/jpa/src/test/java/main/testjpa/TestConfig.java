package main.testjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.TestPropertySource;

@Configuration
@TestPropertySource("application-test.properties")
@EntityScan("main.data.models")
@EnableJpaRepositories(basePackages = "main.jpa")
public class TestConfig {
	public static void main(String[] args) {
		SpringApplication.run(TestConfig.class, args);

	}
}
