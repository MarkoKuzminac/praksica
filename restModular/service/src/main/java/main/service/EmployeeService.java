package main.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import main.data.models.Employee;
import main.data.models.Role;
import main.jpa.EmployeeRepository;

@Service
public class EmployeeService implements IEmployeeService, UserDetailsService {

	private EmployeeRepository empRepo;

	@Autowired
	public EmployeeService(EmployeeRepository empRepo) {

		this.empRepo = empRepo;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Employee user = empRepo.findByUsername(username);
		
		if (user == null) {
			throw new UsernameNotFoundException(username + " was not found");
		}
		System.out.println(user.getUsername());
		System.out.println(user.getPassword());
		//String[] roles = user.getRoles().toArray(new String[user.getRoles().size()]);
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				getAuthorities(user.getRoles()));
		
	}

	private Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (Role role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
		}

		return authorities;
	}

	public void setPassword(String password) {

		empRepo.setPassword(password);
	}

	@Transactional
	public void createEmployee(Employee employee) {
		 empRepo.save(employee);
	}

	public Employee getEmployeeById(Long id) {
		return empRepo.getOne(id);
	}

	@Transactional
	public void deleteEmployeeById(Long id) {
		empRepo.deleteById(id);
	}

	@Transactional
	public void deleteEmployeesByDepartment(Long department_id) {
		empRepo.deleteByDepartment_id(department_id);
	}

	public List<Employee> getAllEmployees() {
		return empRepo.findAll();
	}

	public Employee getHighestSalary() {
		return empRepo.findByMaxSalary();
	}

	@Transactional
	public void changeEmployeesDepartment(Long employee_id, Long dep_id) {
		empRepo.changeEmployeeDepartment(dep_id, employee_id);
	}

}
