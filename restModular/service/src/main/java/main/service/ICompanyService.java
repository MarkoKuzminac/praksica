package main.service;

import java.util.List;

import main.data.models.Company;

public interface ICompanyService {

	List<Company> getAllCompanies();

	void createCompany(Company company);

	void deleteCompany(Long id);

	List<Company> getCompaniesByCity(String city);

}