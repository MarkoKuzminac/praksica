package service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import main.data.models.Company;
import main.data.models.Department;
import main.jpa.CompanyRepository;
import main.service.CompanyService;
import main.service.ICompanyService;

@RunWith(MockitoJUnitRunner.class)
public class CompanyServiceTest {

	@Mock
	private CompanyRepository mockCompanyRepository;

	@InjectMocks
	private ICompanyService companyService = new CompanyService(mockCompanyRepository);

	//private static List<Company> companies;

	@Test
	public void getAllCompaniesTest() {
		List<Company> companies = new ArrayList<Company>();
		Company c1 = new Company("Hooli", "San Diego", "USA", "adress");
		Company c2 = new Company("Google", "Seattle", "USA", "adress");
		companies.add(c1);
		companies.add(c2);
		when(companyService.getAllCompanies()).thenReturn(companies);
		List<Company> companyList = companyService.getAllCompanies();
		
		for(int i=0;i<companyList.size();i++) {
		assertEquals(companyList.get(i), companies.get(i));
		}
		verify(mockCompanyRepository, times(1)).findAll();
	
	}

	@Test
	public void getCompaniesByCity() {
		List<Company> companies = new ArrayList<Company>();
		Company c1 = new Company("Hooli", "San Diego", "USA", "adress");
		companies.add(c1);
		when(companyService.getCompaniesByCity("San Diego")).thenReturn(companies);
		List<Company> companyList = companyService.getCompaniesByCity("San Diego");

		assertEquals(companyList.get(0), companies.get(0));
		verify(mockCompanyRepository, times(1)).findByCity("San Diego");
	}

	@Test
	public void deleteCompanyByIdTest() {
		companyService.deleteCompany(1L);
		verify(mockCompanyRepository, times(1)).deleteById(1l);
	}

	@Test
	public void createDepartmentTest() {
		//List<Company> companies = new ArrayList<Company>();
		Company c1 = new Company("Yahoo", "San Diego", "USA", "adress");
		companyService.createCompany(c1);
		verify(mockCompanyRepository, times(1)).save(c1);
	}
}
