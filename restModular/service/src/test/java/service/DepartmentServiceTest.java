package service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import main.data.models.Department;
import main.jpa.DepartmentRepository;
import main.service.DepartmentService;
import main.service.IDepartmentService;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentServiceTest {

	@Mock
	private DepartmentRepository mockDepartmentRepository;

	@InjectMocks
	private IDepartmentService departmentService = new DepartmentService(mockDepartmentRepository);

	private static List<Department> departments;

	@BeforeClass
	public static void beforeClass() {
		departments=new ArrayList<Department>();
		Department d1 = new Department("IT", 44L);
		Department d2=new Department("HR",55L);
		departments.add(d1);
		departments.add(d2);
	}

	@Test
	public void getDepartmentByNameTest() {
		departmentService.getDepartmentByName("Dunder Mifflin");
		verify(mockDepartmentRepository, times(1)).findByName("Dunder Mifflin");

	}
	@Test
	public void getEmployeeCountByDepartment() {
		departmentService.getEmployeeCountByDepartment("IT");
		verify(mockDepartmentRepository,times(1)).getCount("IT");
	}
	@Test
	public void getAllDepartmentsTest() {
		departmentService.getAllDepartments();
		verify(mockDepartmentRepository,times(1)).findAll();
	
	}
	@Test
	public void changeDepartmentNameTest() {
		departmentService.changeDepartmentName("staro ime", "novo ime");
		verify(mockDepartmentRepository,times(1)).updateDepartmentName("novo ime", "staro ime");
	}
	@Test
	public void deleteDepartmentByNameTest() {
		departmentService.deleteDepartmentByName("imeDepa");
		verify(mockDepartmentRepository,times(1)).deleteByName("imeDepa");
	}
	@Test
	public void createDepartmentTest() {
		Department dep=departments.get(1);
		departmentService.createDepartment(dep);
		verify(mockDepartmentRepository,times(1)).save(dep);
	}
}
