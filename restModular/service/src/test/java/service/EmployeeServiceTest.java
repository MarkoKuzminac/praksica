package service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import main.data.models.Employee;
import main.jpa.EmployeeRepository;
import main.service.EmployeeService;
import main.service.IEmployeeService;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

	@Mock
	private EmployeeRepository mockEmployeeRepository;
	@InjectMocks
	private IEmployeeService employeeService = new EmployeeService(mockEmployeeRepository);

	private static List<Employee> employeesList;

	@BeforeClass
	public static void beforeClass() {
		Employee e1 = new Employee(1l, "Pera Peric", "username", "password", "mail@live.com", new BigDecimal(22345.66));
		Employee e2 = new Employee(2l, "Zika Zikic", "username", "password", "mail@live.com", new BigDecimal(99999.55));
		employeesList = new ArrayList<Employee>();
		employeesList.add(e1);
		employeesList.add(e2);
	}

	@Test
	public void getAllTest() {

		when(employeeService.getAllEmployees()).thenReturn(employeesList);
		List<Employee> employees = employeeService.getAllEmployees();
		assertEquals(2, employees.size());
		verify(mockEmployeeRepository, times(1)).findAll();
		for (int i = 0; i < employees.size(); i++) {
			assertEquals(employeesList.get(i), employees.get(i));

		}

	}

	@Test
	public void getByIdTest() {

		employeeService.getEmployeeById(1l);
		verify(mockEmployeeRepository, times(1)).getOne(1l);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getByIdIllegalArgumentTest() {

		when(employeeService.getEmployeeById(55L)).thenThrow(new IllegalArgumentException());
		employeeService.getEmployeeById(55l);
	}

	@Test
	public void createEmployeeTest() {
		Employee e3 = new Employee(3l, "Jovan Jovanovic Zmaj", "username", "password", "mail@live.com",
				new BigDecimal(22345.66));
		employeeService.createEmployee(e3);
		verify(mockEmployeeRepository, times(1)).save(e3);

	}

	@Test
	public void deleteEmployeeByIdTest() {
		employeeService.deleteEmployeeById(2L);
		verify(mockEmployeeRepository, times(1)).deleteById(2l);
	}

	@Test
	public void getEmployeeByHighestSalaryTest() {
		employeeService.getHighestSalary();
		verify(mockEmployeeRepository, times(1)).findByMaxSalary();
	}
	@Test
	public void deleteEmployeesByDepartmentIdTest() {
		employeeService.deleteEmployeesByDepartment(2L);
		verify(mockEmployeeRepository,times(1)).deleteByDepartment_id(2l);
	}
	@Test
	public void changeEmployeesDepartmentTest() {
		employeeService.changeEmployeesDepartment(2L, 1L);
		verify(mockEmployeeRepository,times(1)).changeEmployeeDepartment(1l, 2l);
	}
}
